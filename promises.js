const btnSallary = document.querySelector('.btn-sallary');
const btnBenefits = document.querySelector('.btn-benefits');

btnSallary.addEventListener('click', showSallary.bind());
btnBenefits.addEventListener('click', showBenefitsAsyncAwait.bind());

const urlCalculations = 'http://localhost:3000/calculations';
const urlBenefits = 'http://localhost:3000/benefits';

function showMassage(message) {
  alert(message)
}

// Fetch data from backend and retreive json
function getDataFromBackend(url) {
  return fetch(url).then(response => response.json())
}

// Use getDataFromBackend() function to show message and handle errors
function showSallary() {
  getDataFromBackend(urlCalculations)
    .then(result => calculateSallary(result))
    .then(
      (sallary) => showMassage(sallary),
      (error) => {
        alert(`Parametr error ${error}`)
        throw error;
      }
    )
    .catch(error => alert(`Catch error ${error}`))
    .finally(() => alert('bye bye! :)'))
}

// Write custom Promise with resolved and rejected state, and use on showSallary() function
function calculateSallary(calculations) {
  const myPromise = new Promise((resolve, reject) => {
    setTimeout(() => {
      if (calculations.workingDays > 10) {
        resolve(calculations.workingHours * calculations.paymentPerHour)
      } else {
        reject(new Error('sorry, you have no access'))
      }
    }, 1000)
  });
  return myPromise;
}

// User Promise all, race
function showBenefits() {
  Promise.all([getDataFromBackend(urlCalculations), getDataFromBackend(urlBenefits)])
    .then(([calculations, benefits]) => {
      return calculateSallary(calculations).then(sallary => {
        return sallary + benefits.premium;
      })
    })
    .then((sallaryWithPremium) => showMassage(sallaryWithPremium))
    .catch(error => alert(`Catch error ${error}`))
}

// Use async await syntax for showBenefits() function
async function showBenefitsAsyncAwait() {
  try {
    const [calculations, benefits] = await Promise.all([getDataFromBackend(urlCalculations), getDataFromBackend(urlBenefits)]);
    const sallary = await calculateSallary(calculations);
    const sallaryWithPremium = sallary + benefits.premium;
    showMassage(sallaryWithPremium);
  } catch (error) {
    alert(`Catch error ${error}`)
  }
}

